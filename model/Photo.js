export class Photo {
  id
  title
  description
  img
  date
  featured

  constructor (props) {
    Object.assign(this, props)
  }

  toJSON () {
    return { ...this }
  }
}
