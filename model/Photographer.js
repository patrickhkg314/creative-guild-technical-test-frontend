export class Photographer {
  name
  bio
  profile_picture
  email
  phone
  album

  constructor (props) {
    Object.assign(this, props)
  }

  toJSON () {
    return { ...this }
  }
}
